//
//  ContentView.swift
//  Component
//
//  Created by 焼肉 on 2021/06/03.
//

import SwiftUI

struct TextButton: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        TextButton()
    }
}
